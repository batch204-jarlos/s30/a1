db.fruits.insertMany([
{
"name": "Banana",
"supplier": "Farmer Fruits Inc.",
"stocks": 30,
"price": 20,
"onSale": true
},
{
"name": "Mango",
"supplier": "Mango Magic Inc.",
"stocks": 50,
"price": 70,
"onSale": true
},
{
"name": "Dragon Fruit",
"supplier": "Farmer Fruits Inc.",
"stocks": 10,
"price": 60,
"onSale": true
},
{
"name": "Grapes",
"supplier": "Fruity Co.",
"stocks": 30,
"price": 100,
"onSale": true
},
{
"name": "Apple",
"supplier": "Apple Valley",
"stocks": 0,
"price": 20,
"onSale": false
},
{
"name": "Papaya",
"supplier": "Fruity Co.",
"stocks": 15,
"price": 60,
"onSale": true
}
]);



//2. Count operator to count the total number of fruits on sale

db.fruits.aggregate([
		{
			$match: {
				"onSale": true
			}
		},
		{
			$count: "totalNumberOfFruitsOnSale"
		}

	]);

//3. Count operator to count the total number of fruits with stock more than 20

db.fruits.aggregate([
		{
			$match: {
				"stocks": {
					$gt: 20
				}
			}
		},
		{
			$count: "totalNumberOfFruitsMoreThan20"
		}
	]);

//4. Average Operator

db.fruits.aggregate([
		{
			$match: {
				"onSale": true,
				"supplier": {
					$nin: ["Apple Valley"]
				}
			}
		},
		{
			$group: {
				_id: "$supplier", avg_price: {$avg: "$price"}
			}
		}

	]);

//5. Max Operator

db.fruits.aggregate([
		{
			$match: {
				"onSale": true
			}
		},
		{
			$group: {
				_id: "$supplier", max_price: {$max: "$price"}
			}
		},
		{
			$sort: {
				max_price: 1
			}
		}
	]);

//6. Min Operator
db.fruits.aggregate([
		{
			$match: {
				"onSale": true
				}
		},
		{
			$group: {
				_id: "$supplier", min_price: {$min: "$price"}
			}
		},
		{
			$sort: {
				min_price: 1
			}
		}
	]);